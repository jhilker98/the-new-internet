---
title: "Privacy"
author: ["Jacob Hilker"]
draft: false
enableToc: true
---

One of the current problems with the internet is the overall lack of privacy that users have. Whether Facebook, Amazon, or Google, it seems that so many of us have gotten used to no real privacy on the internet, despite having the ability to create usernames that conceal who you are. One of the current problems with the internet is the overall lack of privacy that users have. Whether Facebook, Amazon, or Google, it seems that so many of us have gotten used to no real privacy on the internet, despite having the ability to create usernames that conceal who you are. As an example, Google is trying out a new form of tracking technology calld [FLoC](https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea), which a user can't even disable - instead, it forces the server administrator to block that part of the request when you go to a site. Just to show how bad it is, Apple, Amazon, Github, Microsoft, and even Wordpress have all agreed to block this technology. When some of the biggest rivals in technology all agree to block your product when they stand to profit from it, I think it's safe to say that you are on the wrong side.


## The Solution {#the-solution}

My solution would be a blunt, if simple, one. I would ban all cookies that tracked personal information. As for what the regular person can do, install an adblocker like Ublock Origin. As for social media, it's established itself as a behemoth, but you can still try to gain some privacy. On Firefox, there is an extension called "Facebook Container" which isolates your activity on Facebook and other social media from other websites. I'd install that. As for FLoC, I think the solution is very simple - uninstall Chrome, swap to something like Firefox, and never install it again.


### Note From A Developer {#note-from-a-developer}

Although cookies are becoming a thing of the past, there are legitimate uses for them beyond tracking. As an example, this website uses a cookie to remember if you had dark-mode or light-mode enabled, and uses that cookie to set the theme when you next come back to the website. (It's not tracking any personal info, though). In addition, single sign-on uses a cookie (like how you use the UMW login page to access Canvas, Banner, and other sites the university uses), as well as remembering your login. While not all cookies are malicious, so many bad actors have used thm for sending advertisements and malicious that it's hard to separate the harm they do from the good they do.
