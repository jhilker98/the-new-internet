---
title: "Misinformation"
author: ["Jacob Hilker"]
draft: false
enableToc: true
---

I fully admit that I am not qualified to talk very heavily on this subject - I know I have fallen victim to reading something and finding out later that the thing I read happened completely differently from how the article described it, and so on.

I also recognize that there is a difference between accidental misinformation, satire, and something like QAnon.


## The Problem {#the-problem}

I think many of us watched on in horror as the insurrection at the Capitol played out on January 6. One of the thing that I've found shocking is just how many QAnon people were there. I feel that the insurrection speaks loudly enough for what can happen if intentional misinformation is left unchecked for too long, but if it isn't clear enough, enough misinformation that stays out like that leads to violent consequences.


## The Solution {#the-solution}

I am honestly unsure of what the right solution would be for addressing misinformation like the QAnon conspiracy theory, simply because of so many conditions that would need to be met while ensuring anything could be enforcable. I wouldn't want to punish anyone who had accidentally put out misinformation without realizing it, but at the same time, I think that when your misinformation has violent consequences, something has to be done to address it. At the same time, with something like software and code, if a new version is released and some information becomes out of date, I'd want to address that. While not the most helpful solution, I think that just looking further into stories that seem off is the best an individual can do for now. While not ideal, I'm unsure of what else we can really do currently.
