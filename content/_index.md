---
title: "The New Internet"
author: ["Jacob Hilker"]
layout: "single"
draft: false
---

The Internet has been increasingly needed, and we saw this need explode with the pandemic. In addition, having access to so much information at your fingertips makes problem solving for certain issues much easier. However, we have also seen many problems explode from the internet - most prominently, the rise of misinformation and "fake news"; in particular, the QAnon conspiracy theory.

Although the internet has proven to be an incredibly helpful and powerful tool, it has so many problems that it can become hard to see the good in spite of the good it has done. This website aims to explain a few hypothetical ideas to try and improve the internet.
